/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.sms.api;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.model.SmsHistory;
import org.openmrs.module.sms.model.SmsProvider;
import org.openmrs.test.BaseModuleContextSensitiveTest;

/**
 * Tests {@link ${SmsService}}.
 */
public class  SMSModuleServiceTest extends BaseModuleContextSensitiveTest {
	private static SmsService service;
	@Before
	public void init(){
		service = Context.getService(SmsService.class);
	}
	@Test
	public void shouldSetupContext() {		
		assertNotNull(service);
		assertNotNull(Context.getUserService().getAllUsers());
	}
	@Test
	public void getDateSQL(){
//		Date date = service.getDateSQL("select #ssss\r\n * from encounter ", 0);
//		assertNotNull(date);
	}
	@Test
	public void getDateSQL2(){
//		Date date = service.getDateSQL("select * from encounter ", 0);
//		assertNotNull(date);
	}
	@Test
	public void getDateSQ3(){
//		Date date = service.getDateSQL("select \r\n * from encounter", 0);
//		assertNotNull(date);
	}
	@Test
	public void getDateSQ4(){
//		Date date = service.getDateSQL("select \r\n now()", 0);
//		assertNotNull(date);
	}
	//@Test
	public void saveHistory(){
		SmsHistory history = new SmsHistory();
        history.setPatientId(1);
        history.setProgrammingId(1);
        
        //SmsProvider provider = (SmsProvider) service.getPreferredProvider().get((int)Math.random()*service.getPreferredProvider().size());
        //history.setProviderId(provider.getId());
        history.setProviderId(1);
        history.setSentToGatewayDate(new Timestamp(Calendar.getInstance().getTimeInMillis()) );
        history.setSentToGatewayResult("success");
        history.setSmsSentDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        history.setSmsSentResultCode("coderr");
        history.setSmsSentResult("deserr");
        
        service.createHistory(history);
	}	
	
	public static void main(String[] args) {
		String sql = "select #ssss\r\n now();";
		String[] str = sql.split("#\\w+\\r\\n");
		//String sqlEscapeComment = Arrays.toString(str);
		String sqlEscapeComment = sql.replaceAll("#\\w+\\r\\n", "");
		System.out.println(sqlEscapeComment);
	}
}
