<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%@page import="org.openmrs.module.sms.send.SendSms"%>
<%@ include file="/WEB-INF/template/include.jsp" %>
<%@ include file="/WEB-INF/template/header.jsp" %>


<%@ include file="include.jsp" %>

<!-- I have to add the permissions validation before include those files -->
<%@ include file="messages/list.jsp" %>
<%@ include file="messages/smsTest.jsp" %>
<%@ include file="messages/create.jsp" %>
<%@ include file="gateways.jsp" %>
<%@ include file="messages/queue.jsp" %>
<!-- End of verification -->
<!-- Only show when the user is authorized -->
<openmrs:hasPrivilege privilege="View SMS">
<h2>SMS Module</h2>
<span id="hasViewSMSPri" style="display: none"></span>
</openmrs:hasPrivilege>

<openmrs:hasPrivilege privilege="View Test SMS">
<h2 id="ViewTestSMSHeader" style="display: none">SMS Module</h2>
<span id="hasViewTestSMSPri" style="display: none"></span>
</openmrs:hasPrivilege>
<!-- End of header Control -->

<%-- For some reason, I cannot use spring:message here. Don't know why it does not working here --%>
<span class="error" id="unauthorizedError" style="display: none">You do not have permission to view this page</span>


<script type="text/javascript">
	Ext.onReady(function(){
		var contextPath='<%=request.getContextPath()%>';

		// To prevent users for unauthorized access tabs
		if (document.getElementById("hasViewSMSPri") == null && 
			document.getElementById("hasViewTestSMSPri") == null){
			window.location = location.protocol + '//' + window.location.host + contextPath + '/login.htm';
			return;
		}
		if (document.getElementById("hasViewSMSPri") == null){
			document.getElementById("ViewTestSMSHeader").style.display = "";
		}
		// end unauthorized access control
		
		// to hide the error message on document click
		document.onclick = function (e) { 
			document.getElementById("unauthorizedError").style.display = "none";
		};
		
		Ext.QuickTips.init();
		Ext.form.Field.prototype.msgTarget = 'side';

		initStores();
	
		var tabs = new Ext.TabPanel({
			renderTo: 'content',
			resizeTabs: true,
			minTabWidth: 150,
			tabWidth: 180,
			enableTabScroll: true,
			width: 1000,
			height: 10000,
			defaults: {autoScroll: true}
		});
		
		var pageParameters = Ext.urlDecode(window.location.search.substring(1));
		
		// Only add View Test SMS tab if the user has permission
		DWRSmsProgrammingService.getViewSMSTestPermission("View Test SMS", addTestMessagePanel);
		function addTestMessagePanel(hasPermission){
			if (!hasPermission){
				if (pageParameters['do'] == 'message-test'){
					document.getElementById("unauthorizedError").style.display = "";
				}
				
				return;
			}
			// only add the tab when has View Test SMS permission
			try{
				var makeTestPanel = makeMessageTestPanel();
				makeTestPanel.setTitle('<spring:message javaScriptEscape="true" code="sms.test_message" />');				
				var messageTestTab = tabs.add(makeTestPanel);
				if (pageParameters['do'] == 'message-test'){
					messageTestTab.show();
				}
			}catch(error){}
		}
		
		// Only add tabs other than View Test SMS if the user has View SMS permission
		DWRSmsProgrammingService.getViewSMSTestPermission("View SMS", addOtherTestPanel);
		function addOtherTestPanel(hasPermission){
			if (!hasPermission){
				if (pageParameters['do'] == 'message-list' || pageParameters['do'] == 'message-create' ||
						pageParameters['do'] == 'message-queue' || pageParameters['do'] == 'gateways'){
					document.getElementById("unauthorizedError").style.display = "";
				}
				return;
			}

			try {
				var messageListPanel = makeMessageListPanel();
				messageListPanel.setTitle('<spring:message javaScriptEscape="true" code="sms.message_list_programmed" />');
				var messageListTab = tabs.add(messageListPanel);
				if (pageParameters['do'] == 'message-list')
					messageListTab.show();
			} catch(error){}
			

			try {
				var messageCreatePanel = makeMessageCreatePanel();
				messageCreatePanel.setTitle('<spring:message javaScriptEscape="true" code="sms.create_message" />');
				var messageCreateTab = tabs.add(messageCreatePanel);
				if (pageParameters['do'] == 'message-create')
					messageCreateTab.show();
			} catch(error){}
		
			try {
				var gatewaysManagementPanel = makeGatewaysManagementPanel();
				gatewaysManagementPanel.setTitle('<spring:message javaScriptEscape="true" code="sms.manage_gateway" />');
				var gatewaysManagementTab = tabs.add(gatewaysManagementPanel);
				if (pageParameters['do'] == 'gateways')
					gatewaysManagementTab.show();
			} catch(error){}
			
			try {
				var messageQueuePanel = makeMessageQueuePanel();
				messageQueuePanel.setTitle('<spring:message javaScriptEscape="true" code="sms.message_queue" />');
				var messageQueueTab = tabs.add(messageQueuePanel);
				if (pageParameters['do'] == 'message-queue')
					messageQueueTab.show();
			} catch(error){}

		}
		
		
	});
</script>
<br/>

<%@ include file="/WEB-INF/template/footer.jsp"%>
