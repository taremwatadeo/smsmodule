<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%@ include file="/WEB-INF/template/include.jsp" %>

<script>
Ext.ns('Extensive.grid');

Extensive.grid.ItemDeleter = Ext.extend(Ext.grid.RowSelectionModel, {

    width: 55,
    
    sortable: false,
	dataIndex: 0, // this is needed, otherwise there will be an error
    
    menuDisabled: true,
    fixed: true,
    id: 'deleter',
    
    initEvents: function(){
        Extensive.grid.ItemDeleter.superclass.initEvents.call(this);
        this.grid.on('cellclick', function(grid, rowIndex, columnIndex, e){
			if(columnIndex==grid.getColumnModel().getIndexById('deleter')) {
				var store = grid.getStore();
				var record = store.getAt(rowIndex);
				store.remove(record);
				grid.getView().refresh();
			}
        });
    },
    
    renderer: function(v, p, record, rowIndex){
        return '<div class="extensive-remove" style="width: 25px; height: 16px;"><img src="../../moduleResources/sms/delete.png" alt="Delete" /></div>';
    }
});
	
var itemDeleter = new Extensive.grid.ItemDeleter();

function makeMessageQueuePanel(){
	return new Ext.Panel({
		frame: true,
		border: false,
		bodyStyle: 'padding: 0px',
		autoHeight: true,
		autoWidth: true,
		items: [{
			id: 'message-queue',
			xtype: 'grid',
			autoHeight: true,
			colModel: new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
				{id : 'id', header : '<spring:message javaScriptEscape="true" code="sms.id" />', dataIndex : 'id', width:50},
				{id : 'smsName', header : '<spring:message javaScriptEscape="true" code="sms.sms.name" />', dataIndex : 'smsName', width:200},
				{id : 'patientName', header : '<spring:message javaScriptEscape="true" code="sms.patient.name" />', dataIndex : 'patientName', width:200},
				{id : 'noOfAttempt', header : '<spring:message javaScriptEscape="true" code="sms.no.of.attempt" />', dataIndex : 'noOfAttempt'},
				{id : 'lastAttemptDateTime', header : '<spring:message javaScriptEscape="true" code="sms.last.attempt.datetime" />', dataIndex : 'lastAttemptDateTime', width:200},
				itemDeleter]),
			store : Ext.StoreMgr.lookup('message-queue-store'),
			selModel : itemDeleter
			,
			tbar : [ 
			         {
						id: 'remove-all-queue-button',
						xtype: 'button',
						text : '<spring:message javaScriptEscape="true" code="sms.remove.all" />',
						handler : function (){ Ext.StoreMgr.lookup('message-queue-store').removeAll()}
			         }]
		}]
	});
}
</script>