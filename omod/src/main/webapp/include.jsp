<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>

<%@ include file="/WEB-INF/view/module/jslibs/ext.jsp" %>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/css/Spinner.css"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/Spinner.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/SpinnerField.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/statusbar/css/statusbar.css"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/statusbar/StatusBar.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-ux/DwrProxy.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-ux/mif/miframe.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/formUtil.js"/>
<openmrs:htmlInclude file="/moduleResources/sms/scripts/store.js"/>

<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/resources/css/ext-all.css"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/css/RowEditor.css"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/adapter/ext/ext-base.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/ext-all.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/examples/ux/RowEditor.js"/>    
    
<openmrs:htmlInclude file="/dwr/engine.js" />
<openmrs:htmlInclude file="/dwr/util.js" />
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<openmrs:htmlInclude file="/dwr/interface/DWRCohortService.js"/>
<openmrs:htmlInclude file="/dwr/interface/DWRConceptService.js"/>

<openmrs:htmlInclude file="/dwr/interface/DWRSmsProviderService.js"/>
<openmrs:htmlInclude file="/dwr/interface/DWRSmsProgrammingService.js"/>
<openmrs:htmlInclude file="/dwr/interface/DWRSmsMessageQueueService.js"/>
<openmrs:htmlInclude file="/dwr/interface/DWRCohortDefinitionService.js"/>
<openmrs:htmlInclude file="/dwr/interface/DWRPatientSetService.js" />
